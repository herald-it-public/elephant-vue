/* eslint-disable */
import EInput from './Input';
import EButton from './Button';
import EDivider from './Divider';
import EIcon from './Icon';
import ETable from './Table';
import ESelect from './Select';
import EOption from './Option';
import EModal from './Modal';
import ECheckbox from './CheckBox';
import EHeader from './Header';

const components = [
    EInput,
    EButton,
    EDivider,
    EIcon,
    ETable,
    ESelect,
    EOption,
    EModal,
    ECheckbox,
    EHeader,
];

const ElephantVue = {};
ElephantVue.install = function(Vue) {
    for (const component of components) {
        Vue.component(component.name, component);
    }
};

export default ElephantVue;
/* eslint-enable */
